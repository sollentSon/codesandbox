import { findCommand, getAlarm } from './commands.js';
export async function network(msg) {
    switch (msg) {
        case 'будильник':
            return getAlarm();
        default:
            return findCommand(msg);
    }
}
//# sourceMappingURL=network.js.map