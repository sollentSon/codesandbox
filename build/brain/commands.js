import axios from 'axios';
import { getEnding } from './functions/endings.js';
import { checkDistance } from './functions/levenshtein-distance.js';
import { getRandom } from './functions/random.js';
import { sayHello } from './commands/sayHello.js';
import { sayGoodbye } from './commands/sayGoodbye.js';
export async function getAlarm() {
    const hour = new Date().getHours();
    const temp = await axios.get(process.env.WEATHER_API || '')
        .then(res => res.data.main.temp)
        .catch(err => console.log(err));
    return `Доброе утро сэр, за бортом ${hour} час${getEnding(hour)}, температура в городе ${(typeof +temp === 'number' && isFinite(temp)) ?
        `${Math.round(temp)} градус${getEnding(temp)}` :
        `на данный момент неизвестна`}`;
}
export function findCommand(msg) {
    const divided = msg.toLowerCase().split(' ');
    if (divided.length >= 7)
        return 'Бро ты слишком много говоришь... В голове полная либелерда';
    if (divided.length >= 3)
        return 'Пока ничем не могу помочь...';
    if (divided.length <= 3) {
        if (checkDistance(divided, sayHello.command)) {
            return sayHello.phrase[getRandom(0, sayHello.phrase.length - 1)];
        }
        if (checkDistance(divided, sayGoodbye.command)) {
            return sayGoodbye.phrase[getRandom(0, sayGoodbye.phrase.length - 1)];
        }
        return 'На данный момент я этого не знаю';
    }
}
//# sourceMappingURL=commands.js.map