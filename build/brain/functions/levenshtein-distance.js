import { distance } from 'fastest-levenshtein';
export function checkDistance(arr1, arr2) {
    return arr1.length <= arr2.length ?
        isSmallDistance(arr2, arr1) :
        isSmallDistance(arr1, arr2);
}
export function isSmallDistance(arr1, arr2) {
    for (let i = 0; i < arr1.length; i++) {
        for (let j = 0; j < arr2.length; j++) {
            if (distance(arr1[i], arr2[j]) < 3)
                return true;
        }
    }
    return false;
}
//# sourceMappingURL=levenshtein-distance.js.map