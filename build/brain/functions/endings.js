export function getEnding(num) {
    if (num === 0 || num >= 5 && num <= 20)
        return 'ов';
    if (num >= 2 && num <= 4 || num === 22 || num === 23)
        return 'а';
    if (num === 1 || num === 21)
        return '';
}
//# sourceMappingURL=endings.js.map