export const sayGoodbye = {
    command: [
        'пока',
        'прощай',
        'до встречи'
    ],
    phrase: [
        'Пока',
        'До свидания сэр',
        'До встречи сэр'
    ]
};
//# sourceMappingURL=sayGoodbye.js.map