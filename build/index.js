import express from 'express';
import dotenv from 'dotenv';
import cors from 'cors';
import { network } from './brain/network.js';
dotenv.config();
const app = express();
app.use(express.json());
app.use(cors());
const PORT = process.env.PORT || '4001';
const BASE_URL = process.env.BASE_PROD_URL || process.env.BASE_DEV_URL || 'http://localhost:';
app.post('/', async (req, res) => {
    const a = await network(req.body.msg);
    res.status(200).send(`${a}`);
});
app.listen(PORT, () => {
    console.log(`[server]: Server is running at ${BASE_URL}${PORT}`);
});
//# sourceMappingURL=index.js.map