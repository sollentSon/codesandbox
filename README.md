# express-ts-config

## Getting started

This project is a build ready for creating a server using -
[Express](https://expressjs.com)
and
[TypeScript](https://www.typescriptlang.org)
technologies.

## Install dependencies

```
npm install
```

## Run a development server

```
npm run dev
```

## Run a production server

```
npm run start
```

## Build a server

```
npm run start
```

## Check the server for ESLint errors

```
npm run lint
```

## ESLint Bug Fixing

```
npm run format
```
