import { distance } from 'fastest-levenshtein'

export default new class DistanceService {

    private isSmallDistance(arr1: string[], arr2: string[]): boolean {
        for (let i = 0; i < arr1.length; i++) {
            for (let j = 0; j < arr2.length; j++) {
                if (distance(arr1[i], arr2[j]) < 3) return true
            }
        }
        return false
    }

    public checkDistance(arr1: string[], arr2: string[]) {
        return arr1.length <= arr2.length ?
            this.isSmallDistance(arr2, arr1) :
            this.isSmallDistance(arr1, arr2)
    }

    // public checkPhrase(str: string, arr: string[]): boolean {
    //     for (let i = 0; i < arr.length; i++) {
    //         if (distance(str, arr[i]) < 2) return true
    //     }
    //     return false
    // }
}
