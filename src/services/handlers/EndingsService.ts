

export default new class EndingsService {
    public hourEnding(num: number): string {
        if (
            num === 0 ||
            num >= 5 && num <= 20
        ) return 'ов';

        if (
            num >= 2 && num <= 4 ||
            num >= 22
        ) return 'а';

        if (
            num === 1 ||
            num === 21
        ) return '';

        return '';
    }

    public minutesEnding(num: number): string {
        if (
            num === 0 ||
            num >= 5 && num <= 20 ||
            num >= 25 && num <= 30 ||
            num >= 35 && num <= 40 ||
            num >= 45 && num <= 50 ||
            num >= 55 && num <= 60
        ) return '';

        if (
            num >= 2 && num <= 4 ||
            num >= 22 && num <= 24 ||
            num >= 32 && num <= 34 ||
            num >= 42 && num <= 44 ||
            num >= 52 && num <= 54
        ) return 'ы';

        if (
            num === 1 ||
            num === 21 ||
            num === 31 ||
            num === 41 ||
            num === 51
        ) return 'а';

        return '';
    }

    public degreesEnding(number: number): string {
        const num = Math.round(number)

        if (
            num === 0 ||
            num >= 5 && num <= 20 ||
            num >= 25 && num <= 30 ||
            num >= 35 && num <= 40
        ) return 'ов';

        if (
            num >= 2 && num <= 4 ||
            num >= 22 && num <= 23 ||
            num >= 22 && num <= 24 ||
            num >= 32 && num <= 34
        ) return 'а';

        if (
            num === 1 ||
            num === 21 ||
            num === 31 ||
            num === 41
        ) return '';

        return '';
    }
}
