import dayjs from 'dayjs'
import EndingsService from '../handlers/EndingsService.js'

export default new class TimeService {

    private currentHour(): number {
        return  dayjs().hour()
    }

    private currentMinute(): number {
        return dayjs().minute()
    }

    public getCompletedHour(): string {
        const hour = this.currentHour()

        return `${hour} час${EndingsService.hourEnding(hour)}`
    }

    public getCompletedHourWithMinute(): string {
        const minute = this.currentMinute()
        return `${this.getCompletedHour()} ${minute} минут${EndingsService.minutesEnding(minute)}`
    }
}
