import AiApi from '../../repositories/ai/AiApi.js'

export default new class AiService {

    async getAnswer(msg: string) {
        const answer: string | any = await AiApi.fetchAIAnswer(msg)
        console.log(answer)

        if (typeof answer === 'string') {
            return answer.replace(/ChatSonic/g, 'Скамча')
        }
        return 'К сожалению ничем не могу помоч... кажется мне пора передохнуть...'
    }
}
