
import EndingsService from "../handlers/EndingsService.js";
import WeatherApi from "../../repositories/weather/WeatherApi.js";


export default new class WeatherService {

    public async getCompletedDegree() {
        const temp = await WeatherApi.fetchKGDWeather()

        if (typeof +temp === 'number' && isFinite(temp)) {
            return `${Math.round(temp)} градус${EndingsService.degreesEnding(temp)}`;
        }

        return `на данный момент неизвестна`;
    }

}
