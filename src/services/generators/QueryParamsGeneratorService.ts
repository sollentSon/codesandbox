export default new class QueryParamsGeneratorService {
    public generate = (object: any): string => {
        return '?' + new URLSearchParams(object).toString()
    }
}
