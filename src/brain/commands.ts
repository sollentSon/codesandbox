import { getRandom } from '../helpers/random.js'

import DistanceService from '../services/handlers/DistanceService.js'

import TimeService from '../services/topical/TimeService.js'
import WeatherService from '../services/topical/WeatherService.js'
import AiService from '../services/topical/AiService.js'

import { sayHello } from './commands/sayHello.js'
import { sayGoodbye } from './commands/sayGoodbye.js'
import { sayStop } from './commands/sayStop.js'



export async function getAlarm() {
    const time = TimeService.getCompletedHour()
    const temp = await WeatherService.getCompletedDegree()

    return `Доброе утро сэр, за бортом ${time}, температура в городе ${temp}`;
}

export function checkForStop(msg: string): string {
    const divided = msg.toLowerCase().split(' ')

    if (DistanceService.checkDistance(divided, sayStop.command)) {
        return sayStop.phrase
    }
    if (msg.toLowerCase().includes('без звука')) {
        return 'STOP'
    }
    return ''
}

export async function findCommand(msg: string) {
    const divided = msg.toLowerCase().split(' ')

    if (divided.length > 10) return 'Бро ты слишком много говоришь... В голове полная либелерда'

    //if (divided.length > 3) return await AiService.getAnswer(msg)

    if (divided.length <= 3) {

        if (DistanceService.checkDistance(divided, sayHello.command)) {
            return sayHello.phrase[getRandom(0, sayHello.phrase.length - 1)]
        }

        if (DistanceService.checkDistance(divided, sayGoodbye.command)) {
            return sayGoodbye.phrase[getRandom(0, sayGoodbye.phrase.length - 1)]
        }

        return 'На данный момент я этого не знаю'
    }
}
