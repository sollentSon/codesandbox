
import { findCommand, getAlarm, checkForStop} from './commands.js'

interface IMsg {
    msg: string
    isSpeaking: boolean
}

export async function network(body: IMsg) {




    if (body.isSpeaking) {
        return checkForStop(body.msg)
    }

    switch (body.msg) {
        case 'будильник' :
            return await getAlarm()
        default :
            return findCommand(body.msg);
    }
}

