import axios from 'axios'

export default new class AiApi {

    fetchAIAnswer(text: string) {

        return axios.request({
            method: 'POST',
            url: process.env.SONIC_URL,
            headers: {
                accept: 'application/json',
                'content-type': 'application/json',
                'X-API-KEY': process.env.SONIC_KEY
            },
            data: {
                enable_google_results: false,
                enable_memory: false,
                input_text: text
            }
        })
            .then(res => res.data.message)
            .catch(err => console.log(err))

    }
}
