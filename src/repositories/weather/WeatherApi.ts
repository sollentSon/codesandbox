import axios from 'axios'
import QueryParamsGeneratorService from '../../services/generators/QueryParamsGeneratorService.js'

interface IParamsWeather {
    lat: string
    lon: string
    units: string
    appid: string
}

export default new class WeatherApi {

    protected KGD: IParamsWeather
    protected MSQ: IParamsWeather

    constructor() {
        this.KGD = {
            lat: '54.713205',
            lon: '20.528127',
            units: 'metric',
            appid: ''
        }
        this.MSQ = {
            lat: '54.713205',
            lon: '20.528127',
            units: 'metric',
            appid: ''
        }
    }
    public fetchKGDWeather(): Promise<number> {
        this.KGD.appid =  process.env.WEATHER_URL_KEY || ''
        return axios.get((process.env.WEATHER_URL + QueryParamsGeneratorService.generate(this.KGD)))
            .then((res: any) => res.data)
            .then(data => data.main.temp)
            .catch((err) => console.log(err))
    }
}
